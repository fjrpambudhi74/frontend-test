// 1. Palindrome

const isPalindrome = (str) => {
    for (var i = 0, j = str.length-1; i < j; i++ , j--) {
        if (str[i] != str[j]) {
            return false;
        }
    }
    return true;
}


console.log('abcba : ' + isPalindrome('abcba'));

// 2. Bilangan Prima

const findPrimeByRange = (a, b) => {

for (let i = a; i <= b; i++) {
    let count = 0;

    for (let j = 2; j < i; j++) {
        if (i % j == 0) {
            count = 1;
            break;
        }
    }

    if (i > 1 && count == 0) {
        console.log(i);
    }
}
}
findPrimeByRange(11,40);

console.log(findPrimeByRange());

// 3. //


// 4. Count same element

const arr = ["a", "a", "a", "b", "c", "c", "b", "b", "b", "d", "d", "e", "e", "e"];

function count() {

    var current = null;
    var count = 0;
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] != current) {
            if (count > 0) {
                console.log(count + ' , ' + current );
            }
            current = arr[i];
            count = 1;
        } else {
            count++;
        }
    }
    if (count > 0) {
          console.log(count + ' , ' + current );
    }
}

count();

