import Vue from 'vue'
import VueRouter from 'vue-router'
import Products from '../components/Products'
import ProductDetail from '../components/ProductDetail'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Products',
    component: Products
  },

  {
    path: '/product-detail/:id',
    name: 'ProductDetail',
    component: ProductDetail,
    props: true
  },


]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
